import detector as det
from detector import carac
from detector.carac import Images,Combined,Carac 
import numpy as np
import os

class RapidImgListOpeneur(det.ImgListOpeneur):
    combined_file_name = "all_combined.fits"
    def biases( self, subdir=det.Null, **kwargs):
        return self.open(  kwargs.pop("path","PIONIER*BIAS*combined.fits"), subdir, **kwargs)
    def flats( self, subdir=det.Null, **kwargs):
        return self.open( kwargs.pop("path","PIONIER*FLAT*combined.fits"), subdir, **kwargs)
    def darks( self, subdir=det.Null, **kwargs):
        return self.open( kwargs.pop("path","PIONIER*DARK*combined.fits"), subdir, **kwargs)    
    def cubes(self, subdir=det.Null, **kwargs):
        return self.open(  kwargs.pop("path","PIONIER*FLAT*cube.fits") , subdir, **kwargs)
    
#class RapidAllCombinedOpeneur(det.ImgOpeneur):
#    pass    

local = RapidImgListOpeneur( rootdir="/Users/guieu/DETDATA/",
                             wrapper = Images
                         )

dmz = local.new( 
    distant  = True, 
    distdir  = "/data/pionier/",    
    host     = "dmz98.obs.ujf-grenoble.fr", 
    user     = "guieus", 
    password = ";Cham38;"                       
)
##########################
### Change the local rootdir function to the local machine 
machines = { "wbeti":local.new(rootdir="/insroot/SYSTEM/ARCDATA/"),
             "wpnr" :local.new(rootdir="/insroot/SYSTEM/ARCDATA/"),
             "wgoff":local.new(rootdir="/data-vlti/raw/")
}
host = os.getenv("HOST")
if host in machines: local = machines[host]

# opener for ftp conection on beti DETDATA
betidetdata = local.new( 
    distant  = True, 
    distdir  = "/insroot/PIONIER/SYSTEM/DETDATA/",    
    host     = "wbeti.obs.ujf-grenoble.fr", 
    user     = "pnr", 
    password = "Bnice2me",                       
)

# opener for ftp conection on beti /data/PIONIER
betidata = betidetdata.new(
    distdir  ="/data/PIONIER/",
)

#allcomb = RapidAllCombinedOpeneur( 
#     rootdir="/Users/guieu/DETDATA/",
#     wrapper = det.AllCombinedImages,
#     filename = "all_combined.fits"
#)

picnic = det.slicer.DetectorOutput( (128,128), 1 )
rapid  = det.slicer.DetectorOutput( (255,320) ,8 )

output = rapid # just for old code compatibility reason




free  = det.slicer.DetectorIntegratedOpticOutput(  pos=(21,179), outputs=24, 
                                            dispersion=1, 
                                            wollastons=1 , doutputs=8, imgdim=(255,320)                                            
)

freeDark  = det.slicer.DetectorIntegratedOpticOutput(pos=(21,179), 
                                                     outputs=2, 
                                                     dispersion=1, 
                                                     wollastons=1 , 
                                                     doutputs=160, 
                                                     disp_ref=[-10,0],
                                                     imgdim=(255,320)                                                     )
grismDark = freeDark.new(dispersion=7)


grism = free.new( dispersion=6,  disp_ref=[3,0])
woll  = free.new( dispersion=1 ,wollastons=2, dwollastons=[10,0], woll_ref=[-5,0], 
                  disp_ref = [0,0] )

griwoll = woll.new( dispersion=7, disp_ref=[2,0], 
                    wollastons=2, dwollastons=[10,-4], woll_ref=[-5,+2])

freeAC  =   free.new( pos=(free.pos[0]+(8*5), free.pos[1]), outputs=12)
grismAC = freeAC.new(  dispersion=7, disp_ref=[2,0])

wollAC  = freeAC.new( dispersion=1 ,wollastons=2 , dwollastons=[10,0], woll_ref=[-5,0])

griwollAC = wollAC.new( dispersion=7, disp_ref=[2,0], 
                    wollastons=2 , dwollastons=[10,-4], woll_ref=[-5,+2]
)

disp   = {"FREE":free, "WOLL":woll,"GRI+WOL":griwoll,"GRISM":grism}
dispAC = {"FREE":freeAC, "WOLL":wollAC,"GRI+WOL":griwollAC,"GRISM":grismAC}


def test(d,     section= rapid(4,5)): 

    v1 = d.getM2Error(section=section)
    v1 = np.sqrt((v1**2).sum())/float(v1.size)
    
    m2 = d.getMedianM2(section=section)
    m4 = d.getMedianM4(section=section)
    N  = d.getMeanN(section=section, freduce=np.sum)
    print N
    N2 = d.getMeanN(section=section)
    v2 = det.computeVarianceErrorbar(m2, m4, N)
    v3 = det.computeVarianceErrorbar(m2, m4, N2)
    return v1, v2, v3
    
