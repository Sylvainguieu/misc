import glob
import os

from   ftplib import FTP
default_openeur = open

VERBOSE = True  # General verbose
FORCE   = False # force to download already existing file in case of a distant ftp file



def say(verbose, text, level=1):
    if verbose>=level:
        print text
def say2(verbose, text, level=1):
    if verbose>=level:
        print text,

        
def add_slash(directory):
    """ Add slashes to str if not directory.endswith("/")"""
    if not len(directory): return directory
    return directory+"/"*(not directory.endswith("/") )


def lsdir(strglob, verbose=VERBOSE):
    """ lsdir( str )  
    return a list of file/directory in the glob fasion 
    if the glob str start with "@" it points to a file containing 
    a list of path by line, e.g. : "@/home/user/list.txt"
    
    """
    if not issubclass( type(strglob), str):
        raise TypeError('expecting a str object got %s'%(type(strglob)))
    say2( verbose, "Looking for '%s' ..."%strglob)
    if strglob[0]=='@':                 
        files = open(strglob[1:-1]).read().split("\n")
    else:
        files = glob.glob(strglob)
    say( verbose, "%d found"%len(files))
    return files

def new_ftp(host, user, password):
    ftp = FTP(host)
    ftp.login(user, password)
    return ftp


def ftp_lsdir(ftp, strglob, verbose=VERBOSE):
    """
    ftp_lsdir( str, ftp)
    do the same thing than lsdir but for a ftp connection.     
    """
    return ftp_glob_dirlist_rec( ftp, strglob, verbose=verbose)



def ftp_glob_dirlist_rec( ftp, path, verbose=VERBOSE):  
    path_list =  path.split("/")
    pref = ""
    if len(path_list) and path_list[0] == "":
        path_list.pop(0)
        pref= "/"
    say2( verbose, "Looking for '%s:%s' in ftp connection ... "%(ftp.host,path) )
    files=  _ftp_glob_dirlist_rec(ftp, path_list, pref, verbose=verbose)
    say( verbose, "found %d"%(len(files)) )
    return files

def _ftp_glob_dirlist_rec(ftp, path_list, pref="", verbose=VERBOSE):
    # walk through the path_list to get a list of files
    # ftp NLST to not allows to list a path of directory with the * like /tmp/*/*.txt
    # so we need to split the path and goes directory by directory if needed
    
    if not len(path_list):
        return []
    
    directory_glob  = path_list.pop(0)
    if not glob.has_magic( directory_glob ):
        if not len(path_list):
            # end of the recursive call, just return the list
            file_list = []
            ftp.retrlines("NLST %s"%pref+directory_glob , file_list.append)
            return file_list
        else:
            # if no magic found just stick to the prefix and send the rest of
            # path_list as a copy: list(path_list)
            return _ftp_glob_dirlist_rec(ftp, list(path_list), pref=pref+directory_glob+"/", verbose=verbose)
    
    directory_found = []        

    # the following line works only with * not with more complex glob [0-9] etc ...
    #rtr     = ftp.retrlines("NLST %s"%pref+directory_glob , directory_found.append)
    # So we need to list all the directory and then match the files
    ftp.retrlines("NLST %s"%pref, directory_found.append)
    #pref_len = len(pref)
    #directory_found = [l for l in directory_found if glob.fnmatch.fnmatch(l[pref_len:], directory_glob)]
    directory_found = [l for l in directory_found if glob.fnmatch.fnmatch(os.path.split(l)[1], directory_glob)]
    
    if not len(path_list):
        return directory_found
    
    output = []
    
    for d in directory_found:
        # list(path_list) to make a copy 
        fls =  _ftp_glob_dirlist_rec(ftp, list(path_list), pref=d+"/", verbose=verbose)
        # extend the output with the new found
        output.extend(fls)    
    return output


def ftp_dirlist(ftp, directory, verbose=VERBOSE):
    """
    Return a list of file of the directory in ftp connection
    the returned list does not contain the directory path
    """
    listfile = []    
    say(verbose, "Changing distant directory to %s"%(directory))
    
    rtr = ftp.cwd(directory)
    say(verbose,  "%s"%(rtr))
    say(verbose, "Get file list ")
    
    rtr = ftp.retrlines('NLST', listfile.append)
    say(verbose, "%s"%(rtr))
    
    return listfile

def ftp_open_glob( ftp, strglob, localdir= "", distdir="", 
                   opener=default_openeur ,
                   verbose=VERBOSE, force=FORCE, ffilter=None):
    files = ftp_transfer(ftp, strglob, localdir=localdir, distdir=distdir, 
                         verbose=verbose, force=force)
    return open_list(files , opener=opener,verbose=verbose, ffilter=ffilter)
    
    
def ftp_transfer(ftp, strglob, localdir= "",distdir="",                  
                 verbose=VERBOSE, force=FORCE):
    """
    ftp_transfer(ftp, strglob, localdir= "",distdir="")
    Transfer file matching strglob from the ftp connection. The hierarchic path directory 
    will be created from the localdir. 
    distdir is the distant root ftp directory starting point, is equivalent 
    to do a ftp.cwd( distdir) to change directory.
    
    """
    if distdir and len(distdir):
        ftp.cwd(distdir)    
    files = ftp_lsdir(ftp, strglob, verbose=verbose)    
    if not len(files):
        return []
    return ftp_transfer_files( ftp, files, localdir=localdir,
                               distdir=distdir, 
                               verbose=verbose, force=force)

def create_dir(directory,inside="", verbose=VERBOSE):
    """
    recreate is necessary a directory structure from a path string  "a/b/c" or a list ["a","b","c"]
    The second argument precise where the structure should be installed
    so create_dir( "data/set1", "/tmp")  is the same than create_dir( "tmp/data/set1")
    
    """
    
    if not os.path.isdir(inside):
        raise Exception( "'%s' is not a directory"%inside)
    if isinstance( directory, basestring):
        # save time exist if exists
        if os.path.isdir(add_slash(inside)+ directory):
            return None
        directories = directory.split("/")
    else:
        directories = directory
        if os.path.isdir(add_slash(inside)+"/".join(directories)):
            return None
            
    sub = inside
    while len(directories):
        sub += "/"+directories.pop(0)

        if os.path.exists(sub):
            if not os.path.isdir(sub):
                raise Exception("'%s' exists but is not a directory "%(sub))
        else:
            say( verbose, "Creating directory %s"%sub)
            os.mkdir(sub)
    return None
        
    
    

def ftp_transfer_files(ftp,  files, localdir= "", distdir="", verbose=VERBOSE, force=FORCE):
    """
    Transfer a list of files from the ftp connection. The hierarchic path directory 
    will be created from the localdir. 
    distdir is the distant root ftp directory starting point, is equivalent 
    to do a ftp.cwd( distdir) to change directory.    
    
    Return the list of local path to files
    """
    
    if os.path.exists(localdir):
        if not os.path.isdir(localdir):
            raise Exception("The local path '%s' is not a directory"%(localdir))
    else:
        os.mkdir(localdir)
    localdir = add_slash(localdir)
    if distdir and distdir!="":
        ftp.cwd(distdir)
    
    global _ftpfinished
    global _ftpwrfunc
    
       
    outlist = []
    for path in files:
        subdir, fl = os.path.split(path)
        create_dir(subdir, localdir, verbose=verbose)
        localpath = add_slash(localdir)+add_slash(subdir)+fl
        

        if not force and os.path.exists(localpath):
            say(verbose,"file %s already exists use force=True to force download"%(localpath))
        else:
            say(verbose, "FTP: Copying file %s:%s to %s "%(ftp.host, fl, localdir+subdir))
            
            ftp.retrbinary("RETR %s"%(path),open(localpath, "wb").write)
        outlist.append( localpath )
    
    return outlist

def open_list( files, opener=default_openeur , ffilter=None, verbose=VERBOSE):
    fls = []
    for file in files:
        f = open_one(file, opener=opener, ffilter=ffilter,verbose=VERBOSE)
        if f is not None:
            fls.append(f)
    return fls

def open_glob(strglob, ffilter=None, opener=default_openeur, verbose=VERBOSE ):
    files = lsdir( strglob )    
    if len(files) is 0: 
        raise IOError("No file matching '%s'"%(strglob))    
    return open_list( files, opener=opener, ffilter=ffilter, verbose=VERBOSE)
        
def open_one(fl, opener=default_openeur, ffilter=None, verbose=VERBOSE):
    """ open_one(file, opener) 
    is equivalent to opener( fl )  
    The filter keyword is an optional function that takes the file path as one unique 
    argument and should return True if the file is valid to be open or False otherwise    
    """
    
    if ffilter:
        test, fh = ffilter(fl, opener)
        if not test:
            say(verbose, "File '%s' regected by filter"%fl)
            return None
        if fh:
            say(verbose,"File '%s' passed filtering "%fl)
            return fh
    
    say(verbose,"Opening file '%s'"%fl)
    return opener(fl)

try:
    Null
except:
    class Null(object):
        pass
    def isnull(obj):
        return type(obj) == type and issubclass(obj,Null)


class Opener(object):
    distant  = False
    localdir = False  # False means no localdir
    distdir  = ""    
    host     = ""
    user     = ""    
    password = ""
    
    verbose  = VERBOSE
    force    = FORCE
    
    opener   = open
    wrapper  = list

    subdir  = ""
    rootdir = ""
    path    = ""
    
    ffilter = False  # False means no ffilter 
    _params_ = ["distant", "localdir", "distdir", "host", "user", "password",
                "verbose", "force",
                "opener", "wrapper", "subdir" , "rootdir", "path",
                "ffilter"
    ]
 
    def _getattr_(self, attr, val):
        if val is None:
            return getattr(self, attr)            
        return val
    
    def __init__(self, **kwargs):
        """ Open a new List Opener object  kwargs are set to atributes:
        they are:
        distant  = True/False say if it is a distant conection 
        localdir = the local directory to where to copy the files. If None 
                   rootdir is used.        
        distdir  = working directory of the ftp distant conection                       
        
        host, user, password = for ftp connection
        force dowloading ftp file or not 
        
        opener   = the opener function e.g, open 
        wrapper  = the list wrapper 
        
        rootdir = the working root directory (default is the curent directory)
                  if rootdir is set, absolute path will not work anymore. 
        
        subdir  = a subdirectory added to rootdir 
        path    = path can be a str glob, e.i *.text or a file name or a full path
                  any relative path will be treated as /rootdir/subdir/path
                  If path is a absolute path rootdir and subdir are ignored 
        
        ffilter = filter function  
        """
        for attr,value in kwargs.iteritems():
            if not hasattr(self, attr):
                raise KeyError("ImgListOpeneur does not have attribut '%s'"%attr)
            setattr(self, attr, value)
            
    def _parseParams_(self, kwargs, keys=_params_):        
        for key in keys:
            if (key in kwargs) and (kwargs[key] == self._void_):
                kwargs[key] = getattr(self, key)
    def _parseKwargs_(self, args, kwargs, keys):
        
        if len(args)>len(keys):
            raise TypeError("Take at most %d argument"%len(keys))
        output = [] 
        for a in args:
            key = keys.pop(0)
            if key in kwargs:
                raise TypeError("got multiple values for keyword argument '%s' "%key)
            output.append(a)
        for key in keys:
            output.append( kwargs.pop(key, getattr(self, key)))
        return output
    def _checkargs_(self, kwargs):
        if len(kwargs):
            return ""%(kwargs.keys())
        return None
    
    def new(self, **kwargs):
        for k in self._params_:
            kwargs.setdefault( k, getattr(self,k))
        return self.__class__(**kwargs)
    
    def get_path(self, path=None, subdir=None, rootdir=None):
        """
        path(filename=, subdir=, rootdir=)
        Build full path to files
        """
        path    = self._getattr_("path", path)
        subdir  = self._getattr_("subdir", subdir)
        rootdir = self._getattr_("rootdir", rootdir)
        
        if path is None or path=="":
            raise ValueError("File path not specified")
        
        if path.startswith("/"):
            return path
        return add_slash(rootdir)+add_slash(subdir)+path
    
    def get_subdir_path(self,subdir=None, rootdir=None ):
         """
         get_subdir_path( subdir=, rootdir=)
         Return the path of the subdirectory         
         """
         subdir  = self._getattr_("subdir", subdir)
         rootdir = self._getattr_("rootdir", rootdir)                 
         return add_slash(rootdir)+add_slash(subdir)
     
    
    def get_subdir(self, subdir=None):
        subdir  = self._getattr_("subdir", subdir)
        return add_slash(subdir)
        
    
    def get_rootdir(self, rootdir=None):
        rootdir = self._getattr_("rootdir", rootdir)  
        return add_slash(rootdir)

    def get_localdir(self, localdir=None):
        localdir = self._getattr_("localdir", localdir)    
        if localdir is False or localdir is None:
            return self.get_rootdir()
        return add_slash(localdir)
        
    def new_ftp(self, host=None, user=None, password=None):
        host = self._getattr_("host", host)
        user = self._getattr_("user", user)
        password = self._getattr_("password", password)
        return new_ftp(host, user, password)
    
    def open(self, path=None, subdir=None, wrapper=None, distant=None, rootdir=None,
             host=None, user=None, localdir=None, distdir=None, password=None,
             verbose=None, force=None, opener=None, ffilter=None):
        path    = self._getattr_("path", path)
        subdir  = self._getattr_("subdir", subdir)
        rootdir = self._getattr_("rootdir", rootdir)
        wrapper = self._getattr_("wrapper", wrapper)
        distant = self._getattr_("distant", distant)
        host    = self._getattr_("host", host)
        user    = self._getattr_("user", user)
        password = self._getattr_("password", password)   
        distdir  = self._getattr_("distdir", distdir)
        localdir = self._getattr_("localdir", localdir)
        verbose  = self._getattr_("verbose", verbose)
        force    = self._getattr_("verbose", force)
        opener   = self._getattr_("opener", opener)
        ffilter  = self._getattr_("ffilter", ffilter)
        
        if distant:
            ftp      = self.new_ftp( host=host, user=user, password=password)
            localdir = self.get_localdir(localdir)
            
            fs = ftp_open_glob(ftp, self.get_subdir(subdir)+path, localdir=localdir, distdir=distdir, verbose=verbose, force=force, ffilter=ffilter, opener=opener)
        else:
            fs = open_glob( self.get_path( path=path, subdir=subdir, rootdir=rootdir),
                            opener=opener, verbose=verbose, ffilter=ffilter)                               
        return wrapper(fs)

    def transfer(self, path=None, subdir=None, rootdir=None, host=None, user=None, localdir=None, distdir=None, password=None, verbose=None, force=None):
        path    = self._getattr_("path", path)
        subdir  = self._getattr_("subdir", subdir)
        rootdir = self._getattr_("rootdir", rootdir)
        host    = self._getattr_("host", host)
        user    = self._getattr_("user", user)
        password = self._getattr_("password", password)
        distdir  = self._getattr_("distdir", distdir)
        localdir = self._getattr_("localdir", localdir)
        verbose  = self._getattr_("verbose", verbose)
        force    = self._getattr_("verbose", force)
                
        if not host:
            raise Exception("No host set")
        ftp      = self.new_ftp( host=host, user=user, password=password)
        localdir = self.get_localdir(localdir)
        return ftp_transfer(ftp, self.get_subdir(subdir)+path, localdir=localdir, distdir=distdir, verbose=verbose, force=force)

    def lslocal(self, path=None, subdir=None, rootdir=None,verbose=None):
        path    = self._getattr_("path", path)
        subdir  = self._getattr_("subdir", subdir)
        rootdir = self._getattr_("rootdir", rootdir)
        verbose  = self._getattr_("verbose", verbose)
        
        return lsdir( self.get_path( path=path, subdir=subdir, rootdir=rootdir), verbose=verbose )
    
    def lsdistant(self, path=None, subdir=None, distdir=None, host=None, user=None, password=None, verbose=None):
        path    = self._getattr_("path", path)
        subdir  = self._getattr_("subdir", subdir)
        distdir  = self._getattr_("distdir", distdir)
        host    = self._getattr_("host", host)
        user    = self._getattr_("user", user)
        password = self._getattr_("password", password)
        verbose  = self._getattr_("verbose", verbose)
        
        ftp = self.new_ftp(host, user, password)        
        return ftp_lsdir( ftp, add_slash(distdir)+self.get_subdir(subdir)+path, verbose=verbose)
    
    ls = lslocal
    
        

class Test(Opener):
    path = "*.fits"
    opener = staticmethod(lambda x: x)
    rootdir = "/Users/guieu/DETDATA"
    distdir = "/data/PIONIER"
    subdir  = "*" 
    distant = True
    host = "wbeti"
    user = "pnr"
    password = "Bnice2me"
    
    


    
            
