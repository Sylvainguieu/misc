################################# 
# version 1.0  
# Author S.Guieu
# History 
# Todo:
# 
#  - Add a possibility of masked_array
#  - axis=["x","y"] is not compatible with nparray wich accept only tuples for axis !!!! 
import numpy as np
VERBOSE = 1

class IndexAxesOutOfBound(Exception):
    pass

def isdataxlist(lst):
    """
    Return True if all the element of the a list are DataX object with the 
    same axes names 
    """
    first = True
    for data in lst:
        if not isdatax(data): return False
        if first:
            axes = data.axes
            first = False
        else:
            if axes!=data.axes: return False
    return True
def isdatax(data):
    #return isinstance( data, DataX)
    
    ###
    # use this in debug mode 
    if not isinstance(data, np.ndarray):
        return False
    return hasattr( data, "axes")

def dataxconcat(lst, newaxis=None, check=True):
    """
    Convert a flat list of DataX object into one DataX
    the newaxis keyword is the name of the added axis. 
    if check = True (default) all element of the list are checked 
    to be compatible. If false assume that they all are and the first 
    element is taken as data type and shape. 
    """
    if newaxis is None:
        return np.asarray(lst)
    if not len(lst): 
        return DataX( np.asarray(lst), [newaxis])    
    if check:
        if not isdataxlist(lst):
            return DataX( [ np.asarray(data) for data in lst]  , [newaxis])
        else:
            return DataX(  [ np.asarray(data) for data in lst] , [newaxis]+list(lst[0].axes))
    
    return DataX( [ np.asarray(data) for data in lst] , [newaxis]+list(lst[0].axes))




def _broadcast_axis(sec, axis, axes1, array_axes, axes2, ia, i):
    # this cause a problem 
    # np.random.random( (100, 256, 320))[ np.array([1,2,3]), np.array([[1,2,3],[1,2,3]]) ].shape
    #
    
    if isinstance(sec, slice):
        #case of slice conserve the axes as it is
        if array_axes is None:
            return sec, axes1+[axis], array_axes, axes2, ia
        else:
            return sec, axes1, array_axes, axes2+[axis], ia
    if isinstance(sec, (int,long)):
        #case of integer the axes is lost 
        return sec, axes1, array_axes, axes2, ia+1 if ia is not None else None
    if isdatax(sec):
        # Take the axes of datax has new axes (array_axes) 
        # If array_axes already exists check compatibility
        # and return empty list
        if array_axes is not None:            
            if array_axes[2] and sec.axes != array_axes[0]:
                raise ValueError("axes mismatch: objects cannot be broadcast to a single axes name: %s!=%s"%(sec.axes,array_axes))
            array_axes = (array_axes[0] , max( array_axes[1], len(sec.shape)), True)  
            if (i-ia)>1:
                # array indexing in a not contigus order
                return sec, [],  array_axes, axes1+axes2, i
            else:
                return sec, axes1, array_axes, axes2, i
                
        return sec, axes1, (sec.axes, len(sec.shape), True), axes2, i
    else:
        # Everything else should be a list or ndarray 
        # 
        sec = np.array( sec )
        if array_axes is not None:
            if array_axes[2]: #axes are given by a previous datax
                array_axes = (array_axes[0] , max( array_axes[1], len(sec.shape)), True)                                
            else:
                array_axes = (array_axes[0]+[axis], max( array_axes[1], len(sec.shape)), False)
            
            if (i-ia)>1:
                # array indexing in a not contigus order
                return sec, [],  array_axes, axes1+axes2, i
            else:
                return sec, axes1,  array_axes, axes2, i         
        return sec, axes1, ([axis],len(sec.shape), False) , axes2, i
    
def _broadcast_axes( sections, axes):
    axis1  = []
    axis2  = []
    array_axes = None
    ia = None
    for i,(sec,axis) in enumerate(zip(sections, axes)):
        sec, axis1, array_axes, axis2,ia = _broadcast_axis( sec,axis,  axis1, array_axes, axis2, ia, i)
    
    array_axes = _reform_array_axes (*array_axes[0:2]) if array_axes is not None else []
    return axis1+array_axes+axis2
    return tuple(newsections), newaxes

def _reform_array_axes( axes, N):
    if N==len(axes): return axes
    if N>len(axes):
        if len(axes)>1:
            return [ (tuple(axes), i) for i in range(N) ]
        else:
            return [ (axes[0], i) for i in range(N) ]
    
    return [tuple(axes)]
    

def o_broadcast_axis(sec, axis, array_axis):
    # this cause a problem 
    # np.random.random( (100, 256, 320))[ np.array([1,2,3]), np.array([[1,2,3],[1,2,3]]) ].shape
    #
    
    if isinstance(sec, slice):
        #case of slice conserve the axis as it is 
        return sec, [axis], array_axis
    if isinstance(sec, (int,long)):
        #case of integer the axis is lost 
        return sec, [], array_axis
    if isdatax(sec):
        # Take the axes of datax has new axes (array_axis) 
        # If array_axis already exists check compatibility
        # and return empty list
        if array_axis is not None:
            if sec.axes != array_axis:
                raise ValueError("axes mismatch: objects cannot be broadcast to a single axes name: %s!=%s"%(sec.axes,array_axis))
            return sec, [], array_axis
        return sec, sec.axes, sec.axes
    else:
        # Everything else should be a list or ndarray 
        # 
        sec = np.array( sec )
        if array_axis is not None:
            return sec, [], array_axis
        array_axis = [ (axis,i) for i in range(len(sec.shape)) ]
        return sec, array_axis, array_axis
    
def o_broadcast_axes( sections, axes):
    newaxes     = []
    newsections = []
    array_axis = None
    for sec,axis in zip(sections, axes):
        sec, axis, array_axis = _broadcast_axis( sec, axis, array_axis)
        newaxes += axis
        newsections.append(sec)
    return tuple(newsections), newaxes
    

def _decore_loose_axes(func):
    def decored_loose_axes(datax, *args, **kwargs):
        return func(np.asarray( datax), *args, **kwargs)
    decored_loose_axes.__doc__ = "The call of this function will forget the axes labels\n"+func.__doc__
    return decored_loose_axes

def _decore_reduce_func(reduce_func):
    def decorated_reduce_func( datax, *args, **kwargs):
        # assume first of args is always axis, is that true ?
        if len(args) and "axis" in kwargs:
            raise TypeError("%s got multiple values for keyword argument 'axis'"%ufunc)
        axis = args[0] if len(args) else kwargs.pop("axis", None)
        return datax.reduce_axis( reduce_func, axis=axis, **kwargs)
    
    decorated_reduce_func.__doc__ = "same as the numpy equivalent except that the axes label are conserved. Operation can be made if at least one \n\n"+reduce_func.__doc__
    
    return decorated_reduce_func


def _prepare_op(left, right):
    """
    prepare two DataX objects for ufunc operations
    if both left and right are DataX array, the dimensions with the same 
    axes label are used for the operations. The return object in this case 
    will have the axes label of the object with the largest number of axes. 
    This function prepare the left and right operators in this way 
    """
    revers = False
    if len(left.axes)< len(right.axes):
        right, left = left, right
        revers = True
    
    if left.axes == right.axes[-len(left.axes):]:
        return left, right, left.axes, left.axes

    newrightaxes = [ a for a in left.axes if a in right.axes ]

    newleftaxes = []
    for n in left.axes:
        if not n in newrightaxes: 
            newleftaxes.insert(0, n)
        else:
            newleftaxes.append(n)    
    if revers:
        right.transpose(newrightaxes), left.transpose( newleftaxes ),newrightaxes, right.axes 

    return left.transpose(newleftaxes), right.transpose( newrightaxes ), newleftaxes, left.axes 

def size(A, axis=None):
    """ Same as numpy.size but list of axis is implemented """
    if axis is None:
        return A.size
    axes = A.axes if isdatax(A) else range(len(A.shape)) 
    if isinstance( axis, (tuple,list)):
        return reduce( lambda x,y: x*np.size(A,axis=axes.index(y)), axis, 1)
    return np.size(A, axis=axes.index(axis))
def size_of_shape( shape, axes, axis=None):
    if axis is None:
        return reduce( lambda x,y: x*y, shape, 1)
    if isinstance( axis, (tuple,list)):
        return reduce( lambda x,y: x*shape[axes.index(y)], axis, 1)
    return shape[axes.index(axis)]
    
def __lop__(op):
    """ binary left operator decorator """
    def tmpop(self,right): 
        return self._run_op(self, right, op)
    return tmpop
def __rop__(op): 
    """ binary right operator decorator """
    def tmpop(self,left): 
        return self._run_op(left, self, op)
    return tmpop
def __uop__(op):
    """ unary operator decorator """
    def tmpop(self):
        return self.__class__(op(np.asarray(self)), list(self.axes))
    return tmpop


class DataX(np.ndarray):
    _verbose = None # if None use the VERBOSE module default 
    _apply_funclist = ["idx", "section", "reduce"]
    def __new__(subtype,data_array, axes=None, rec=False):
        # Create the ndarray instance of our type, given the usual
        # ndarray input arguments.  This will call the standard
        # ndarray constructor, but return an object of our type.
        # It also triggers a call to InfoArray.__array_finalize__

        obj = np.asarray(data_array).view(subtype)

        #obj = np.ndarray.__new__(subtype, shape, dtype, buffer, offset, strides,
        #                 order)
        # set the new 'info' attribute to the value passed
        if axes is None:
            axes = range( len(obj.shape))
        elif isinstance(axes, str):
            axes = [axes,]
        
        elif len(axes)>len(obj.shape):
            raise KeyError("len(axes) must be inferior or equal to the len(shape) of created ndarray" )
        if len( set(axes))!=len(axes):
            raise KeyError("All axes labels must be unique")
        obj.axes = axes
        
        # Finally, we must return the newly created object:
        return obj

    def __array_finalize__(self, obj):
        # ``self`` is a new object resulting from
        # ndarray.__new__(DataX, ...), therefore it only has
        # attributes that the ndarray.__new__ constructor gave it -
        # i.e. those of a standard ndarray.
        #
        # We could have got to the ndarray.__new__ call in 3 ways:
        # From an explicit constructor - e.g. DataX():
        #    obj is None
        #    (we're in the middle of the DataX.__new__
        #    constructor, and self.info will be set when we return to
        #    DataX.__new__)
        if obj is None: return
        # From view casting - e.g arr.view(DataX):
        #    obj is arr
        #    (type(obj) can be DataX)
        # From new-from-template - e.g infoarr[:3]
        #    type(obj) is DataX
        #
        # Note that it is here, rather than in the __new__ method,
        # that we set the default value for 'info', because this
        # method sees all creation of default objects - with the
        # DataX.__new__ constructor, but also with
        # arr.view(InfoArray).
        self.axes = getattr(obj, 'axes', range(len(obj.shape)))        
        # We do not need to return anything
    
    @property
    def A(self):
        return np.asarray(self)
    
    def axis_len(self, axis):
        """
        a.axis_len(axis) -> len of the given axis
        if axis is None return a.size
        """
        if axis is None:
            return self.size
        return self.shape[self.axis_index(axis)]
    
    def axis_index(self, axislabel, num=None):
        """
        If num is None return the first index and raise IndexError if label not found 
        If num is int return the label of the numth axes but raise IndexAxesOutOfBound
        if not found in the list or out of bound (e.i. num to large)

        If axislabel is a tuple num keyword is ignored and is replaced by the second
        argument of the tuple
        """

        return self.axes.index(axislabel)
        
        if isinstance(axislabel, tuple):
            if len(axislabel)==1: 
                axislabel, num = axislabel[0],0
            elif len(axislabel)==2:
                axislabel, num = axislabel
            else:
                raise ValueError("If tuple len(axis) must be <=2 ")
        
        if num is None: 
            try:
                return self.axes.index(axislabel)
            except ValueError:
                raise IndexError("Wrong axis label : %s"%(axislabel))
        
        num = int(num) if num is not None else 0        
        iaxes = [i for i,ax in enumerate(self.axes) if ax==axislabel]
        if not len(iaxes):
            raise IndexAxesOutOfBound("Wrong axis label : %s"%(axislabel))        
        try:
            return iaxes[num]
            #return iaxes.index(num)+offset
        except IndexError:
            raise IndexAxesOutOfBound("Axis label out of bound : %s,%d"%(axislabel,num))
    
    def get_missing_axes( self, lst):
        return [ axis for axis in self.axes if axis not in lst]
    
    def get_axes(self,lst=None, alternate=None):
        if alternate is None:
            alternate = self.axes

        
        if lst is None: return list(self.axes)
        lst = list(lst)
        cNone = lst.count(None)

        if cNone:
            if cNone>1:
                raise ValueError("Axes list allows only one None value")
            iNone = lst.index(None)
            lst.remove(None)
            lst = lst[0:iNone]+self.get_missing_axes(lst)+lst[iNone:]
        
        for axis in lst: 
            if not axis in self.axes:
                return ValueError("wrong axis label %s"%axis)
        return lst

    def __array_wrap__(self, out_arr, context=None):
        #print 'In __array_wrap__:'
        #print '   self is %s' % repr(self)
        #print '   arr is %s' % repr(out_arr)
        # then just call the parent
        return np.ndarray.__array_wrap__(self, out_arr, context)

    def __array_prepare__(self, obj, context=None):
        #if context:
        #    if isinstance(obj, DataX):
        #        left, right, axes, oaxes = _prepare_op(self, obj)
        #print obj.shape        
        #print "context ", context
        return obj
    
    def _allsection(self):
        return [ slice(0,None) for a in self.axes ]        
    
    def _section(self, allsection, axesname, section):
        allsection[self.axis_index(axesname)] = section    
        if isinstance(section, int):
            return False
        return True
    
    def idx(self, section, axis=None):
        ###
        # To be optimized !!!!!
        # For a DataX
        # %timeit data[i]
        # 10000 loops, best of 3: 38.5 us per loop
        # For a regular array 
        # In [622]: %timeit data.A[ia]
        # 100000 loops, best of 3: 4.36 us per loop
        # c
        if not isinstance(section,tuple):
            raise ValueError("first argument must be a tuple, got %s"%type(section))
        if axis is None:
            return self[section]
        
        N = len(section)
        if len(axis) != N:
            raise ValueError( "axis keyword should have the same len than section tuple")
        if not N:
            return self    
        if len(set(axis))!=len(axis):
            raise ValueError("All element of the axis list must be unique got %s"%axis)
        
        axes_index = [self.axis_index(ax) for ax in axis]
        N = max(axes_index)
        # build an empty list of section according to the max index 
        allsection = [slice(0,None)]*(N+1)
        for sec,i in zip(section, axes_index):
            allsection[i] = sec
        return self[tuple(allsection)]
    
    
    def section(self, section, axis=None):
        allsection = self._allsection()
        allsection[self.axis_index(axis)] = section
        
        
        axes = list( self.axes) # copy of axes
        arout = np.asarray(self)[tuple(allsection)]
        
        if len(arout.shape)<len(self.shape):
            axes.remove(axis)
        if len(arout.shape)>len(self.shape):
            i = axes.index(axis)
            axes.remove(axis)
            axes = axes[0:i]+[(axis,i) for i in range(len(arout.shape)-len(self.shape)+1)]+axes[i+1:]
            
            #axes = axes[0:i]+[axis]*( len(arout.shape)-len(self.shape))+axes[i:]        
        return self.__class__( np.asarray(self)[tuple(allsection)], axes )

    def section_s(self, axesname_sec):
        # axesname_sec is a list of tuple [(axesname1, sec1), (axesname2, sec2), ....]
        if not len(axesname_sec): return self
        if isinstance( axesname_sec, dict):
            axesname_sec = axesname_sec.iteritems()
        
        allsection = self._allsection()
        axes = list( self.axes) # copy of axes
        for axesname, section in axesname_sec:
            if not self._section( allsection, axesname, section):
                axes.remove(axesname)        
        return self.__class__( np.asarray(self)[tuple(allsection)], axes )
    
    def __str__(self):
        return "%s(\n%s,\naxes=%s)"%(self.__class__.__name__, str(np.asarray(self)), str(self.axes))
    def __repr__(self):
        return "%s(\n%s,\naxes=%s)"%(self.__class__.__name__, np.asarray(self).__repr__(), str(self.axes))
        
    def __getitem__(self, items):        
        if not isinstance(items, tuple):
            items = (items,)
        Naxes = len(self.axes)
        Nitem = len(items)
        newaxis = _broadcast_axes(items, self.axes)+self.axes[Nitem:]
        
        if len(newaxis):
             return self.__class__(np.asarray(self)[items], newaxis)
        return np.asarray(self)[items]
                    

    def _get_verbose(self):
        """
        return self._verbose or VERBOSE if None
        """
        return self._verbose if self._verbose is not None else VERBOSE
    @classmethod
    def _run_op(cls, left, right, op):
        if isdatax(right) and isdatax(left):
            left, right, axes, oaxes = _prepare_op(left, right)            
            return cls(  op( np.asarray(left), np.asarray(right)), axes).transpose( oaxes)
        if isdatax(left):
            return cls(  op(np.asarray(left), right) , list(left.axes))
        if isdatax(right):
            return cls(  op(left, np.asarray( right)) , list(right.axes))
        return cls( op( left, right) )  
        
    def _prepare_transform(self, reshapes, ignore_unknown):
        """
        From a list of axes name and formulae return a tuple containing:
        a list to pass to tranpose, a list new pass to a reshape and a list of new axes name.       
        """
        datashape = self.shape
        newshape   = []
        transposes = []
        newaxes    = []        
        allkeys = []
        
        for i,x in enumerate(list(reshapes)): #list make a copy 
            if isinstance(x, tuple):
                if len(x)<2:
                    raise ValueError( "If axes def is tuple must be of len>1")
                
                if ignore_unknown:                    
                    x2 = list(x[0:1])+[k for k in x[1:] if (k in self.axes) or (k is None)]
                    if len(x2)>1:
                        reshapes[i] = tuple(x2)
                    else:
                        reshapes.remove(x)
                        i -= 1
                else:
                    x2 = x                    
                allkeys.extend(x2[1:])
            else:
                if ignore_unknown and (not x in self.axes) and (x is not None):
                    reshapes.remove(x)
                    i -= 1
                else:
                    allkeys.append(x) 
        
        if allkeys.count(None):
            if allkeys.count(None)>1:
                raise ValueError( "None appears more than ones")
            allkeys.remove(None)
            
        
        if None in reshapes:
            iNone = reshapes.index(None)
            reshapes.remove(None)
            reshapes = reshapes[0:iNone]+self.get_missing_axes(allkeys)+reshapes[iNone:]
                
        for x in reshapes:                                                
            if isinstance(x, tuple):
                if len(x)<2:
                    raise ValueError( "If axes def is tuple must be of len>1")
                newname = x[0]
                merged_axis = list(x[1:])
                if None in merged_axis:                    
                    iNone = merged_axis.index(None)
                    merged_axis.remove(None)
                    merged_axis =  merged_axis[0:iNone]+self.get_missing_axes(allkeys)+merged_axis[iNone:]
                
                indexes = [ self.axis_index(s) for s in merged_axis ]
                transposes.extend(merged_axis)                
                newshape.append(reduce(lambda x, y: x*y, [datashape[i] for i in indexes]) )
                newaxes.append(newname)
            
            else:                                        
                i = self.axis_index(x)
                transposes.append( x )
                newaxes.append( x )
                newshape.append( datashape[i] )
        
        return tuple(transposes), tuple(newshape), newaxes

    
    def apply(self, **kwargs):
        #allactions = {k:kwargs.pop(k,  {}) for k in  self._apply_funclist}                
        # 
        #for ax in self.axes:
        #    for action_key in allactions:
        #        lazykwargs = "%s_%s"%(ax,action_key)
        #        if lazykwargs in kwargs:
        #            allactions[action_key][ax] = kwargs[lazykwargs]
        # 
        verbose = self._get_verbose()
        Nfunc = len(self._apply_funclist)
        ifunc = 0
        #for funckey in self._apply_funclist:
        while ifunc<Nfunc:
            funckey = self._apply_funclist[ifunc]
            axes = list(self.axes)
            
            f = funckey
            args    = {}
            notargs = []

            for ax in self.axes:
                lazykwargs = "%s_%s"%(ax,funckey)
                if lazykwargs in kwargs:
                    args[ax] = kwargs.pop(lazykwargs)

            if not len(args):
                # if there is no fancy keyword in kwargs go to the next func
                # because the self.axes can have changed we need to continue
                # until there is no fancy keyword available
                ifunc += 1                
                continue
                    
            if funckey== "idx":
                sec = []
                sec_axis = []
                for ax in axes:
                    if ax in args and ax in self.axes:
                        sec.append(args[ax])
                        sec_axis.append(ax)                                        
                self = self.idx(tuple(sec), axis=sec_axis)
                ########
                # At this point self can be something else
                # like a float for instance 
                if not isdatax( self): return self
            else:
                for ax in axes:
                    
                    if ax in args and ax in self.axes:
                        self = getattr(self, f)( args[ax], axis=ax )
                        ########
                        # At this point self can be something else
                        # like a float for instance 
                        if not isdatax( self): return self

                        
        if verbose>=2 and len(kwargs):
            print "NOTICE : keys %s had no effect on data"%kwargs.keys()
        return self


    
    def transform(self, reshapes, ignore_unknown=False):
        """
        dx.transform( axes ) 
        Transform the array axes according to the axes list of axes label or tuple. 
        transform can make a transpose and reshape in the same call.
        If one element is a tuple, it first item should be the new axes label, the othes the label
        of existing axes of the array which will be concatenated together. 

        e.g.:
        
        > d = datax( det.np.random.rand( 8, 10, 12,13), ["time", "z", "y", "x"])
        
        > d.transform( [ "z", ("xy", "x", "y"), "time"] )
        DataX(([[...,        
               [ 0.82653106,  0.99736293,  0.67030048, ...,  0.91404063,
                 0.71512099,  0.20758938]]]),('z', 'xy', 'time'))
        # Equivalent to :
        > DataX ( np.asarray(d.transpose( [1,3,2,0] )).reshape( ( 10,13*12,8) ), ["z", "xy", "time"])
                
        """
        transposes, newshape, newaxes = self._prepare_transform(reshapes, ignore_unknown)
        data = self.transpose( transposes ).reshape( newshape )        
        return self.__class__( data, newaxes ) 
    
    @property
    def T(self):
        return self.transpose()

    def transpose(self, tps=None, *args, **kwargs):        
        if tps is None:
            tps = list(self.axes)
            tps[0], tps[-1] = tps[-1], tps[0] ## swap first and last axes
        
        tps = self.get_axes(tps)        
        return self.__class__(  np.asarray(self).transpose( [self.axis_index(tp) for tp in tps ]), list(tps))
    transpose.__doc__ = np.transpose.__doc__
    
    def true_axes(self):
        names = []
        nums  = []
        for a in self.axes:
            nums.append( names.count(a))
            names.append(a) 
        return zip(names, nums)

    reshape =  _decore_loose_axes(np.reshape)
    ravel   =  _decore_loose_axes(np.ravel)
    flatten =  _decore_loose_axes(np.ndarray.flatten)
    
    def reduce_axis(self, freduce, axis=None, **kwargs):        
        # Loop over axis
        if isinstance(axis,list):
            for ax in self.get_axes(axis):
                #if not isinstance(self,DataX): return self
                # initial is None after the first iteration
                self = self.reduce_axis(freduce, axis=ax, **kwargs)
            return self        
        ##########################################################

    

        if axis is None:
            if kwargs.get("keepdims",False):
                return self.__class__(freduce( np.asarray(self), axis = None, **kwargs ), self.axes)
            else:
                return freduce( np.asarray(self), axis = None, **kwargs )
        
        iaxis = self.axis_index(axis)            
        ndata = freduce(np.asarray(self), axis = iaxis, **kwargs )
        axes = list(self.axes)        
        if len(ndata.shape) < len(self.shape):
            axes.remove( axis )             
        elif len(ndata.shape) > len(self.shape):
            raise Exception("The freduce function cannot add dimension to the data")                 
        if not len(ndata.shape): return ndata
        return self.__class__(ndata, axes)
    
    def reduce_func(self, freduce, axis=None,  initial=None):
        #########################################################
        # Loop over axis
        if isinstance(axis,list):
            for ax in self.get_axes(axis):
                if not isdatax(self): return self
                # initial is None after the first iteration
                self = self.reduce_func(freduce, axis=ax, initial= (None if axis.index(ax) else initial) )
            return self        
        ##########################################################
    
        if axis is None: 
            if initial is None:
                return reduce(freduce, self.flat)
            else:
                return reduce(freduce, self.flat, initial)
        axes = list(self.axes)
        if initial is None:
            return reduce (freduce, self.transpose( [axis,None] ))
        else:
            return reduce (freduce, self.transpose( [axis,None] ), initial)
    
    def reduce(self, freduce, axis=None, initial=None):
        """ reduce the data of an axis name with the freduce func
        Try first to call freduce(data, axis=axis), if this return 
        a TypeError, because axis=  is not set, try to use the builtin
        reduce function on the appropriate axes. 
        If axes is None the data is flattened before beiing called.

        If you want to use explicitaly one or the other method use 
        the reduce_axis for 
        """
        #########################################################
        # loop over axis
        if isinstance(axis,list):
            if initial is None:
                try: 
                ##
                # To avoid confusion try first on the first axes 
                # if succed do the rest  
                    tmp = self.reduce_axis(freduce, axis=axis[0])
                    self = tmp
                    return self.reduce_axis( freduce, axis=axis[1:])            
                except TypeError:
                    return self.reduce_func(freduce, axis=axis)
            else:
                return self.reduce_func(freduce, axis=axis, initial=initial)
        ##########################################################
        
        if initial is None:
            try: 
                return self.reduce_axis(freduce, axis=axis)
            except TypeError:
                return self.reduce_func(freduce, axis=axis)
        return self.reduce_func(freduce, axis=axis, initial=initial)
    
    mean = _decore_reduce_func(np.mean)
    var  = _decore_reduce_func(np.var)
    std  = _decore_reduce_func(np.std)
    min  = _decore_reduce_func(np.min)
    max  = _decore_reduce_func(np.max)
    sum  = _decore_reduce_func(np.sum)
    prod = _decore_reduce_func(np.prod)
    
    argmax = _decore_reduce_func(np.argmax)
    argmin = _decore_reduce_func(np.argmin)
    
    cumsum =  _decore_reduce_func(np.cumsum)
    cumprod = _decore_reduce_func(np.cumprod)
    
    @__lop__        
    def __add__(x, y):
        return x+y
    @__lop__
    def __sub__(x, y):
        return x-y
    @__lop__
    def __mul__(x, y):
        return x*y
    @__lop__
    def __floordiv__(x, y):
        return x//y
    @__lop__
    def __mod__(x, y):
        return x%y
    @__lop__
    def __divmod__(x, y):
        return divmod(x,y)
    @__lop__
    def __pow__(x, y ):
        return pow(x,y)
    @__lop__
    def __lshift__(x, y):
        return x<<y
    @__lop__
    def __rshift__(x, y):
        return x>>y
    @__lop__
    def __and__(x, y):
        return x&y
    @__lop__
    def __xor__(x, y):
        return x^y
    @__lop__
    def __or__(x, y):
        return x|y
    
    @__rop__        
    def __radd__(x, y):
        return x+y
    @__rop__
    def __rsub__(x, y):
        return x-y
    @__rop__
    def __rmul__(x, y):
        return x*y
    @__rop__
    def __rdiv__(x, y):
        return x/y
    @__rop__
    def __rtruediv__(x, y):
        return x/y
    @__rop__
    def __rfloordiv__(x, y):
        return x/y
    @__rop__
    def __rmod__(x, y):
        return x%y
    @__rop__
    def __rdivmod__(x, y):
        return divmod(x,y)
    @__rop__
    def __rpow__(x, y ):
        return pow(x,y)
    @__rop__
    def __rlshift__(x, y):
        return x<<y
    @__rop__
    def __rrshift__(x, y):
        return x>>y
    @__rop__
    def __rand__(x, y):
        return x&y
    @__rop__
    def __rxor__(x, y):
        return x^y
    @__rop__
    def __ror__(x, y):
        return x|y
    
    @__uop__
    def __neg__(x):
        return -x
    @__uop__
    def __pos__(x):
        return +x
    @__uop__
    def __abs__(x):
        return abs(x)
    @__uop__
    def __invert__(x):
        return ~x
    @__uop__    
    def __complex__(x):
        return complex(x)
    @__uop__    
    def __int__(x):
        return int(x)
    @__uop__    
    def __long__(x):
        return long(x)
    @__uop__    
    def __float__(x):
        return float(x)
    @__uop__
    def __index__(x):
        return x.__index__()

def datax( data, axis=None):
    return DataX( data, axis)


d = DataX( [
    [ [ [ 1111, 1112, 1113, 1114, 1115], 
        [ 1121, 1122, 1123, 1124, 1125], 
        [ 1131, 1132, 1133, 1134, 1135], 
        [ 1141, 1142, 1143, 1144, 1145] ], 
      [ [ 1211, 1212, 1213, 1214, 1215], 
        [ 1221, 1222, 1223, 1224, 1225], 
        [ 1231, 1232, 1233, 1234, 1235], 
        [ 1241, 1242, 1243, 1244, 1245] ] ],
    [ [ [ 2111, 2112, 2113, 2114, 2115], 
        [ 2121, 2122, 2123, 2124, 2125], 
        [ 2131, 2132, 2133, 2134, 2135], 
        [ 2141, 2142, 2143, 2144, 2145] ], 
      [ [ 2211, 2212, 2213, 2214, 2215], 
        [ 2221, 2222, 2223, 2224, 2225], 
        [ 2231, 2232, 2233, 2234, 2235], 
        [ 2241, 2242, 2243, 2244, 2245] ] ],
    [ [ [ 3111, 3112, 3113, 3114, 3115], 
        [ 3121, 3122, 3123, 3124, 3125], 
        [ 3131, 3132, 3133, 3134, 3135], 
        [ 3141, 3142, 3143, 3144, 3145] ], 
      [ [ 3211, 3212, 3213, 3214, 3215], 
        [ 3221, 3222, 3223, 3224, 3225], 
        [ 3231, 3232, 3233, 3234, 3235], 
        [ 3241, 3242, 3243, 3244, 3245] ] ]
]
    
    

    
           , axes=["expo","time","y","x"])


#idata = np.random.random( (100, 255, 320))
#axes =  _broadcast_axes( (slice(0,5),
#                                    #DataX([[1,2,3]],["toto","tata"]),
#                                    [1,2,3],
#                                    [[1,2,3]]  ) , ["a","b","c"])
#idata = DataX( idata,  ["a","b","c"])
#data = DataX(idata[sections], axes)



#import rapid as r
#reload(r.det) and reload(r)
#section = r.free()
#d = r.det.CubeImage2(r.local.open( "PIONIER_DARK-FF-allcombined.fits") )
#print section[0].axes
#print DataX(d[1].data, d[1].axes).idx ( (section[0],section[1]) , axis=["y","x"]).axes
